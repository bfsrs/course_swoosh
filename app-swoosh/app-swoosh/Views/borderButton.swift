//
//  borderButton.swift
//  app-swoosh
//
//  Created by benny franke on 21.01.18.
//  Copyright © 2018 showRescueService. All rights reserved.
//

import UIKit

class borderButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 3.0
        layer.borderColor = UIColor.white.cgColor
    }

}
